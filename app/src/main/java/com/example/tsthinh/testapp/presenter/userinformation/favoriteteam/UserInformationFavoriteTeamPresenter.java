package com.example.tsthinh.testapp.presenter.userinformation.favoriteteam;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.tsthinh.testapp.model.userinformation.favoriteteam.FavoriteTeamModel;
import com.example.tsthinh.testapp.view.userinformation.favoriteteam.IUserInfoFavoriteTeamView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

/**
 * Created by tsthinh on 11/8/2016.
 */

public class UserInformationFavoriteTeamPresenter implements IUserInformationFavoriteTeamPresenter {
    final String TAG = "FavoriteTeamPresenter";
    private Context context;
    private IUserInfoFavoriteTeamView userInfoFavoriteTeamView;
    private ArrayList<FavoriteTeamModel> favoriteTeamModels;

    public UserInformationFavoriteTeamPresenter(Context context, IUserInfoFavoriteTeamView userInfoFavoriteTeamView) {
        this.context = context;
        this.userInfoFavoriteTeamView = userInfoFavoriteTeamView;
        Intent intent = ((Activity) context).getIntent();
        favoriteTeamModels = (ArrayList<FavoriteTeamModel>) intent.getSerializableExtra("favorite_team");
        Gson gson = new GsonBuilder().create();
        Log.d(TAG, "Favorite teams: " + gson.toJson(favoriteTeamModels));
    }

    public ArrayList<FavoriteTeamModel> getFavoriteTeamModels() {
        return favoriteTeamModels;
    }
}

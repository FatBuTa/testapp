package com.example.tsthinh.testapp.view.userinformation.education;

/**
 * Created by thinh on 11/8/2016.
 */

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tsthinh.testapp.home.R;
import com.example.tsthinh.testapp.model.userinformation.education.EducationModel;
import com.example.tsthinh.testapp.model.userinformation.education.School;

import java.util.ArrayList;

public class UserInfoEducationAdapter extends RecyclerView.Adapter<UserInfoEducationAdapter.ViewHolder> {
    private ArrayList<EducationModel> mDataset;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtType;
        public TextView txtName;

        public ViewHolder(View v) {
            super(v);
            txtType = (TextView) v.findViewById(R.id.school_type);
            txtName = (TextView) v.findViewById(R.id.school_name);
        }
    }

    public void add(int position, EducationModel dataItem) {
        mDataset.add(position, dataItem);
        notifyItemInserted(position);
    }

    public UserInfoEducationAdapter(ArrayList<EducationModel> myDataset) {
        if (myDataset != null)
            mDataset = myDataset;
        else
            mDataset = new ArrayList<>();
    }

    @Override
    public UserInfoEducationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_info_education_item, parent, false);
        UserInfoEducationAdapter.ViewHolder vh = new UserInfoEducationAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(UserInfoEducationAdapter.ViewHolder holder, int position) {
        final EducationModel name = mDataset.get(position);
        holder.txtType.setText(mDataset.get(position).getType());
        holder.txtName.setText(mDataset.get(position).getSchool().getName());
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}

package com.example.tsthinh.testapp.model.userinformation;

import com.example.tsthinh.testapp.model.userinformation.education.EducationModel;
import com.example.tsthinh.testapp.model.userinformation.favoriteteam.FavoriteTeamModel;
import com.example.tsthinh.testapp.model.userinformation.work.JobModel;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by tsthinh on 11/7/2016.
 */

public class UserInformationModel implements Serializable {

    private String id = "";
    private String name = "";
    private String birthday = "";
    private String email = "";
    private Picture picture;
    private String link = "";
    private String gender = "";
    private String political = "";
    private String publicKey = "";
    private String quotes = "";
    private String relationship_status = "";
    private String religion = "";
    private String third_party_id = "";
    private String website = "";
    private ArrayList<JobModel> work;
    private ArrayList<EducationModel> education;
    private ArrayList<FavoriteTeamModel> favorite_teams;

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(final String birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public Picture getPicture() {
        return picture;
    }

    public void setPicture(final Picture picture) {
        this.picture = picture;
    }

    public String getLink() {
        return link;
    }

    public void setLink(final String link) {
        this.link = link;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(final String gender) {
        this.gender = gender;
    }

    public String getPolitical() {
        return political;
    }

    public void setPolitical(final String political) {
        this.political = political;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(final String publicKey) {
        this.publicKey = publicKey;
    }

    public String getQuotes() {
        return quotes;
    }

    public void setQuotes(final String quotes) {
        this.quotes = quotes;
    }

    public String getRelationship_status() {
        return relationship_status;
    }

    public void setRelationship_status(final String relationship_status) {
        this.relationship_status = relationship_status;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(final String religion) {
        this.religion = religion;
    }

    public String getThird_party_id() {
        return third_party_id;
    }

    public void setThird_party_id(final String third_party_id) {
        this.third_party_id = third_party_id;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(final String website) {
        this.website = website;
    }

    public ArrayList<JobModel> getWork() {
        return work;
    }

    public void setWork(final ArrayList<JobModel> work) {
        this.work = work;
    }

    public ArrayList<EducationModel> getEducation() {
        return education;
    }

    public void setEducation(final ArrayList<EducationModel> education) {
        this.education = education;
    }

    public ArrayList<FavoriteTeamModel> getFavorite_teams() {
        return favorite_teams;
    }

    public void setFavorite_teams(final ArrayList<FavoriteTeamModel> favorite_teams) {
        this.favorite_teams = favorite_teams;
    }
}

package com.example.tsthinh.testapp.view.userinformation.work;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.tsthinh.testapp.home.R;
import com.example.tsthinh.testapp.model.userinformation.work.JobModel;
import com.example.tsthinh.testapp.presenter.userinformation.work.IUserInformationWorkPresenter;
import com.example.tsthinh.testapp.presenter.userinformation.work.UserInformationWorkPresenter;
import com.example.tsthinh.testapp.view.userinformation.CustomItemDecoration;

import java.util.ArrayList;

/**
 * Created by tsthinh on 11/8/2016.
 */

public class UserInfoWorkActivity extends AppCompatActivity implements IUserInfoWorkView {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    IUserInformationWorkPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info_work);
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new CustomItemDecoration());
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        presenter = new UserInformationWorkPresenter(this, this);
        ArrayList<JobModel> myDataset = presenter.getJobModels();
        mAdapter = new UserInfoWorkAdapter(myDataset);
        mRecyclerView.setAdapter(mAdapter);
    }
}

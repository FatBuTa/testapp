package com.example.tsthinh.testapp.presenter.userinformation.education;

import com.example.tsthinh.testapp.model.userinformation.education.EducationModel;
import com.example.tsthinh.testapp.model.userinformation.education.School;

import java.util.ArrayList;

/**
 * Created by tsthinh on 11/9/2016.
 */

public interface IUserInformationEducationPresenter {
    public ArrayList<EducationModel> getEducationModels();
}

package com.example.tsthinh.testapp.presenter.userinformation;

import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;

/**
 * Created by tsthinh on 11/9/2016.
 */

public interface IUserInformationPresenter {
    public void onShowPhoto(ImageView imageView);
    public void onShowId(TextView textView);
    public void onShowName(TextView textView);
    public void onShowBirthday(TextView textView);
    public void onShowEmail(TextView textView);
    public void onShowGender(TextView textView);
    public void onShowLink(TextView textView);
    public void onShowPolitical(TextView textView);
    public void onShowPublicKey(TextView textView);
    public void onShowQuotes(TextView textView);
    public void onShowRelationshipStatus(TextView textView);
    public void onShowReligion(TextView textView);
    public void onShowThirdPartyId(TextView textView);
    public void onShowWebsite(TextView textView);
    public void onShowEducation();
    public void onShowWorkExperiment();
    public void onShowFavoriteTeams();
}
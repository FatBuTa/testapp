package com.example.tsthinh.testapp.model.userinformation;

import java.io.Serializable;

/**
 * Created by tsthinh on 11/9/2016.
 */

public class PictureData implements Serializable {
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }
}

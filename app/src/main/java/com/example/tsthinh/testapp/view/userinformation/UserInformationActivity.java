package com.example.tsthinh.testapp.view.userinformation;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tsthinh.testapp.home.R;
import com.example.tsthinh.testapp.presenter.userinformation.IUserInformationPresenter;
import com.example.tsthinh.testapp.presenter.userinformation.UserInformationPresenter;
import com.gc.materialdesign.views.ButtonFlat;

/**
 * Created by tsthinh on 11/7/2016.
 */

public class UserInformationActivity extends AppCompatActivity implements IUserInformationView {

    private ImageView photoView;
    private TextView textViewId;
    private TextView textViewName;
    private TextView textViewBirthday;
    private TextView textViewEmail;
    private TextView textViewGender;
    private TextView textViewLink;
    private TextView textViewPolitical;
    private TextView textViewPublicKey;
    private TextView textViewQuotes;
    private TextView textViewRelationshipStatus;
    private TextView textViewReligion;
    private TextView textViewThirdPartyId;
    private TextView textViewWebsite;
    private ButtonFlat buttonEducation;
    private ButtonFlat buttonWork;
    private ButtonFlat buttonFavoriteTeams;
    IUserInformationPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        photoView = (ImageView) findViewById(R.id.photo);
        textViewId = (TextView) findViewById(R.id.user_id);
        textViewName = (TextView) findViewById(R.id.user_name);
        textViewBirthday = (TextView) findViewById(R.id.user_birthday);
        textViewEmail = (TextView) findViewById(R.id.user_email);
        textViewGender = (TextView) findViewById(R.id.user_gender);
        textViewLink = (TextView) findViewById(R.id.user_link);
        textViewPolitical = (TextView) findViewById(R.id.user_political);
        textViewPublicKey = (TextView) findViewById(R.id.user_public_key);
        textViewQuotes = (TextView) findViewById(R.id.user_quotes);
        textViewRelationshipStatus = (TextView) findViewById(R.id.user_relationship_status);
        textViewReligion = (TextView) findViewById(R.id.user_religion);
        textViewThirdPartyId = (TextView) findViewById(R.id.user_third_party_id);
        textViewWebsite = (TextView) findViewById(R.id.user_website);
        buttonEducation = (ButtonFlat) findViewById(R.id.user_info_education);
        buttonWork = (ButtonFlat) findViewById(R.id.user_info_work);
        buttonFavoriteTeams = (ButtonFlat) findViewById(R.id.user_info_favorite_team);
        presenter = new UserInformationPresenter(this, this);
        presenter.onShowPhoto(photoView);
        presenter.onShowId(textViewId);
        presenter.onShowName(textViewName);
        presenter.onShowBirthday(textViewBirthday);
        presenter.onShowEmail(textViewEmail);
        presenter.onShowGender(textViewGender);
        presenter.onShowLink(textViewLink);
        presenter.onShowPolitical(textViewPolitical);
        presenter.onShowPublicKey(textViewPublicKey);
        presenter.onShowQuotes(textViewQuotes);
        presenter.onShowRelationshipStatus(textViewRelationshipStatus);
        presenter.onShowReligion(textViewReligion);
        presenter.onShowThirdPartyId(textViewThirdPartyId);
        presenter.onShowWebsite(textViewWebsite);

        buttonEducation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onShowEducation();
            }
        });

        buttonFavoriteTeams.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onShowFavoriteTeams();
            }
        });

        buttonWork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onShowWorkExperiment();
            }
        });
    }
}

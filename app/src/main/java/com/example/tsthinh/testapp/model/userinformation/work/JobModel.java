package com.example.tsthinh.testapp.model.userinformation.work;

import java.io.Serializable;

/**
 * Created by tsthinh on 11/8/2016.
 */

public class JobModel implements Serializable {
    private String description;
    private Employer employer;
    private Location location;
    private Position position;
    private String start_date;
    private String end_date;

    public JobModel(final String description, final Employer employer, final Location location,
                    final Position position, final String startDate, final String endDate) {
        this.description = description;
        this.employer = employer;
        this.location = location;
        this.position = position;
        this.start_date = startDate;
        this.end_date = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Employer getEmployer() {
        return employer;
    }

    public void setEmployer(final Employer employer) {
        this.employer = employer;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(final Location location) {
        this.location = location;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(final Position position) {
        this.position = position;
    }

    public String getStartDate() {
        return start_date;
    }

    public void setStartDate(final String startDate) {
        this.start_date = startDate;
    }

    public String getEndDate() {
        return end_date;
    }

    public void setEndDate(final String endDate) {
        this.end_date = endDate;
    }
}

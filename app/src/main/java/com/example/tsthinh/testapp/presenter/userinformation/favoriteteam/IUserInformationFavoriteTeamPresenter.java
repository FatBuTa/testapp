package com.example.tsthinh.testapp.presenter.userinformation.favoriteteam;

import com.example.tsthinh.testapp.model.userinformation.favoriteteam.FavoriteTeamModel;

import java.util.ArrayList;

/**
 * Created by tsthinh on 11/9/2016.
 */

public interface IUserInformationFavoriteTeamPresenter {
    public ArrayList<FavoriteTeamModel> getFavoriteTeamModels();
}

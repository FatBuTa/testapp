package com.example.tsthinh.testapp.presenter.userinformation.education;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.tsthinh.testapp.model.userinformation.education.School;
import com.example.tsthinh.testapp.model.userinformation.education.EducationModel;
import com.example.tsthinh.testapp.view.userinformation.education.IUserInfoEducationView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

/**
 * Created by tsthinh on 11/8/2016.
 */

public class UserInformationEducationPresenter implements IUserInformationEducationPresenter{
    final String TAG = "EducationPresenter";
    private Context context;
    private IUserInfoEducationView userInfoEducationView;
    private ArrayList<EducationModel> educationModels;

    public UserInformationEducationPresenter(Context context, IUserInfoEducationView userInfoEducationView) {
        this.context = context;
        this.userInfoEducationView = userInfoEducationView;
        Intent intent = ((Activity) context).getIntent();
        educationModels = (ArrayList<EducationModel>) intent.getSerializableExtra("education");
        Gson gson = new GsonBuilder().create();
        Log.d(TAG, "Education: " + gson.toJson(educationModels));
    }

    public ArrayList<EducationModel> getEducationModels() {
        return educationModels;
    }
}

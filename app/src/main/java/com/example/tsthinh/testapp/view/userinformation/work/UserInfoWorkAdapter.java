package com.example.tsthinh.testapp.view.userinformation.work;

/**
 * Created by tsthinh on 11/8/2016.
 */


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tsthinh.testapp.home.R;
import com.example.tsthinh.testapp.model.userinformation.work.JobModel;

import java.util.ArrayList;

public class UserInfoWorkAdapter extends RecyclerView.Adapter<UserInfoWorkAdapter.ViewHolder> {
    private ArrayList<JobModel> mDataset;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtDescription;
        public TextView txtEmployer;
        public TextView txtLocation;
        public TextView txtPosition;
        public TextView txtStartDate;
        public TextView txtEndDate;
        public TextView txtJob;

        public ViewHolder(View v) {
            super(v);
            txtDescription = (TextView) v.findViewById(R.id.description);
            txtEmployer = (TextView) v.findViewById(R.id.employer);
            txtLocation = (TextView) v.findViewById(R.id.location);
            txtPosition = (TextView) v.findViewById(R.id.position);
            txtStartDate = (TextView) v.findViewById(R.id.start_date);
            txtEndDate = (TextView) v.findViewById(R.id.end_date);
            txtJob = (TextView) v.findViewById(R.id.job);
        }
    }

    public void add(int position, JobModel dataItem) {
        mDataset.add(position, dataItem);
        notifyItemInserted(position);
    }

    public UserInfoWorkAdapter(ArrayList<JobModel> myDataset) {
        if (myDataset != null)
            mDataset = myDataset;
        else
            mDataset = new ArrayList<>();
    }

    @Override
    public UserInfoWorkAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_info_work_item, parent, false);
        UserInfoWorkAdapter.ViewHolder vh = new UserInfoWorkAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(UserInfoWorkAdapter.ViewHolder holder, int position) {
        final JobModel name = mDataset.get(position);
        holder.txtDescription.setText(mDataset.get(position).getDescription());
        holder.txtEmployer.setText(mDataset.get(position).getEmployer().getName());
        holder.txtLocation.setText(mDataset.get(position).getLocation().getName());
        holder.txtPosition.setText(mDataset.get(position).getPosition().getName());
        holder.txtStartDate.setText(mDataset.get(position).getStartDate());
        holder.txtEndDate.setText(mDataset.get(position).getEndDate());
        position++;
        holder.txtJob.setText("Job " + position + ": ");
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}


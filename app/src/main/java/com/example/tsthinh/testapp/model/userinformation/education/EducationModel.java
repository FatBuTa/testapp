package com.example.tsthinh.testapp.model.userinformation.education;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by tsthinh on 11/8/2016.
 */

public class EducationModel implements Serializable {
    private School school;
    private String type;
    private String id;

    public School getSchool() {
        return school;
    }

    public void setSchool(final School school) {
        this.school = school;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }
}

package com.example.tsthinh.testapp.presenter.home;

import com.facebook.CallbackManager;
import com.facebook.login.widget.LoginButton;

/**
 * Created by tsthinh on 11/9/2016.
 */

public interface IHomePresenter {
    public void registerCallbackListener(LoginButton loginButton, CallbackManager callbackManager);
}

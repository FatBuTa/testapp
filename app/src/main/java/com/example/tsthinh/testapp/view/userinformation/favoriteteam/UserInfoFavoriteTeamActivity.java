package com.example.tsthinh.testapp.view.userinformation.favoriteteam;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.tsthinh.testapp.home.R;
import com.example.tsthinh.testapp.model.userinformation.favoriteteam.FavoriteTeamModel;
import com.example.tsthinh.testapp.presenter.userinformation.favoriteteam.IUserInformationFavoriteTeamPresenter;
import com.example.tsthinh.testapp.presenter.userinformation.favoriteteam.UserInformationFavoriteTeamPresenter;
import com.example.tsthinh.testapp.view.userinformation.CustomItemDecoration;

import java.util.ArrayList;

/**
 * Created by thinh on 11/8/2016.
 */

public class UserInfoFavoriteTeamActivity extends AppCompatActivity implements IUserInfoFavoriteTeamView {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    IUserInformationFavoriteTeamPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info_favorite_team);
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new CustomItemDecoration());
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        presenter = new UserInformationFavoriteTeamPresenter(this, this);
        ArrayList<FavoriteTeamModel> myDataset = presenter.getFavoriteTeamModels();
        mAdapter = new UserInfoFavoriteTeamAdapter(myDataset);
        mRecyclerView.setAdapter(mAdapter);
    }
}


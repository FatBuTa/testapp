package com.example.tsthinh.testapp.view.userinformation.education;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.tsthinh.testapp.home.R;
import com.example.tsthinh.testapp.model.userinformation.education.EducationModel;
import com.example.tsthinh.testapp.model.userinformation.education.School;
import com.example.tsthinh.testapp.presenter.userinformation.education.IUserInformationEducationPresenter;
import com.example.tsthinh.testapp.presenter.userinformation.education.UserInformationEducationPresenter;
import com.example.tsthinh.testapp.view.userinformation.CustomItemDecoration;

import java.util.ArrayList;

/**
 * Created by thinh on 11/8/2016.
 */

public class UserInfoEducationActivity extends AppCompatActivity implements IUserInfoEducationView {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    IUserInformationEducationPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info_education);
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new CustomItemDecoration());
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        presenter = new UserInformationEducationPresenter(this, this);
        ArrayList<EducationModel> myDataset = presenter.getEducationModels();
        mAdapter = new UserInfoEducationAdapter(myDataset);
        mRecyclerView.setAdapter(mAdapter);
    }
}

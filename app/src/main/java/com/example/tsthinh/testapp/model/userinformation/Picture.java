package com.example.tsthinh.testapp.model.userinformation;

import java.io.Serializable;

/**
 * Created by tsthinh on 11/9/2016.
 */

public class Picture implements Serializable {
    private PictureData data;

    public PictureData getData() {
        return data;
    }

    public void setData(final PictureData data) {
        this.data = data;
    }
}

package com.example.tsthinh.testapp.presenter.userinformation.work;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.tsthinh.testapp.model.userinformation.work.JobModel;
import com.example.tsthinh.testapp.view.userinformation.work.IUserInfoWorkView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

/**
 * Created by tsthinh on 11/8/2016.
 */

public class UserInformationWorkPresenter implements IUserInformationWorkPresenter{
    final String TAG = "WorkPresenter";
    private Context context;
    private IUserInfoWorkView userInfoWorkView;
    private ArrayList<JobModel> jobModels;

    public UserInformationWorkPresenter(Context context, IUserInfoWorkView userInfoWorkView) {
        this.context = context;
        this.userInfoWorkView = userInfoWorkView;
        Intent intent = ((Activity) context).getIntent();
        jobModels = (ArrayList<JobModel>) intent.getSerializableExtra("work");
        Gson gson = new GsonBuilder().create();
        Log.d(TAG, "Work: " + gson.toJson(jobModels));
    }

    public ArrayList<JobModel> getJobModels() {
        return jobModels;
    }
}

package com.example.tsthinh.testapp.model.userinformation.work;

import java.io.Serializable;

/**
 * Created by tsthinh on 11/9/2016.
 */

public class Location implements Serializable{
    private String id;
    private String name;

    public String toString() {
        return "Location(id = " + id + ", name = " + name + ")";
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

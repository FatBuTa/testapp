package com.example.tsthinh.testapp.presenter.userinformation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tsthinh.testapp.model.userinformation.UserInformationModel;
import com.example.tsthinh.testapp.view.userinformation.IUserInformationView;
import com.example.tsthinh.testapp.view.userinformation.education.UserInfoEducationActivity;
import com.example.tsthinh.testapp.view.userinformation.favoriteteam.UserInfoFavoriteTeamActivity;
import com.example.tsthinh.testapp.view.userinformation.work.UserInfoWorkActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

/**
 * Created by tsthinh on 11/7/2016.
 */

public class UserInformationPresenter implements IUserInformationPresenter {
    final String TAG = "UserInformation";
    private Context context;
    private IUserInformationView userInformationView;
    private UserInformationModel userInfo;

    public UserInformationPresenter(Context context, IUserInformationView userInformationView) {
        this.context = context;
        this.userInformationView = userInformationView;
        Intent intent = ((Activity) context).getIntent();
        userInfo = (UserInformationModel) intent.getSerializableExtra("UserInformation");
        Gson gson = new GsonBuilder().create();
        Log.d(TAG, "User info: " + gson.toJson(userInfo));
    }

    @Override
    public void onShowPhoto(ImageView imageView) {
        Log.d(TAG, "onShowPhoto");
        if (userInfo != null) {
            String photoUrl = userInfo.getPicture().getData().getUrl();
            Log.d(TAG, "url: " + photoUrl);
            Picasso.with((Context) userInformationView).load(photoUrl).into(imageView);
        }
    }

    @Override
    public void onShowId(TextView textView) {
        Log.d(TAG, "onShowId");
        String info = "Id: \t" + userInfo.getId();
        Log.d(TAG, info);
        textView.setText(info);
    }

    @Override
    public void onShowName(TextView textView) {
        Log.d(TAG, "onShowName");
        String info = "Name: \t" + userInfo.getName();
        Log.d(TAG, info);
        textView.setText(info);
    }

    @Override
    public void onShowBirthday(TextView textView) {
        Log.d(TAG, "onShowBirthday");
        String info = "Birthday: \t" + userInfo.getBirthday();
        Log.d(TAG, info);
        textView.setText(info);
    }

    @Override
    public void onShowEmail(TextView textView) {
        Log.d(TAG, "onShowEmail");
        String info = "Email: \t" + userInfo.getEmail();
        Log.d(TAG, info);
        textView.setText(info);
    }

    @Override
    public void onShowGender(TextView textView) {
        Log.d(TAG, "onShowGender");
        String info = "Gender: \t" + userInfo.getGender();
        Log.d(TAG, info);
        textView.setText(info);
    }

    @Override
    public void onShowLink(TextView textView) {
        Log.d(TAG, "onShowLink");
        String info = "Link: \t" + userInfo.getLink();
        Log.d(TAG, info);
        textView.setText(info);
    }

    @Override
    public void onShowPolitical(TextView textView) {
        Log.d(TAG, "onShowPolitical");
        String info = "Political: \t" + userInfo.getPolitical();
        Log.d(TAG, info);
        textView.setText(info);
    }

    @Override
    public void onShowPublicKey(TextView textView) {
        Log.d(TAG, "onShowPublicKey");
        String info = "PublicKey: \t" + userInfo.getPublicKey();
        Log.d(TAG, info);
        textView.setText(info);
    }

    @Override
    public void onShowQuotes(TextView textView) {
        Log.d(TAG, "onShowQuotes");
        String info = "Quotes: \t" + userInfo.getQuotes();
        Log.d(TAG, info);
        textView.setText(info);
    }

    @Override
    public void onShowRelationshipStatus(TextView textView) {
        Log.d(TAG, "onShowRelationshipStatus");
        String info = "Relationship Status: \t" + userInfo.getRelationship_status();
        Log.d(TAG, info);
        textView.setText(info);
    }

    @Override
    public void onShowReligion(TextView textView) {
        Log.d(TAG, "onShowReligion");
        String info = "Religion: \t" + userInfo.getReligion();
        Log.d(TAG, info);
        textView.setText(info);
    }

    @Override
    public void onShowThirdPartyId(TextView textView) {
        Log.d(TAG, "onShowThirdPartyId");
        String info = "Third Party Id: \t" + userInfo.getThird_party_id();
        Log.d(TAG, info);
        textView.setText(info);
    }

    @Override
    public void onShowWebsite(TextView textView) {
        Log.d(TAG, "onShowWebsite");
        String info = "Website: \t" + userInfo.getWebsite();
        Log.d(TAG, info);
        textView.setText(info);
    }

    @Override
    public void onShowEducation() {
        Log.d(TAG, "onShowEducation");
        Intent intent = new Intent(context, UserInfoEducationActivity.class);
        if (userInfo != null)
            intent.putExtra("education", userInfo.getEducation());
        context.startActivity(intent);
    }

    @Override
    public void onShowWorkExperiment() {
        Log.d(TAG, "onShowWorkExperiment");
        Intent intent = new Intent(context, UserInfoWorkActivity.class);
        if (userInfo != null)
            intent.putExtra("work", userInfo.getWork());
        context.startActivity(intent);
    }

    @Override
    public void onShowFavoriteTeams() {
        Log.d(TAG, "onShowFavoriteTeams");
        Intent intent = new Intent(context, UserInfoFavoriteTeamActivity.class);
        if (userInfo != null)
            intent.putExtra("favorite_team", userInfo.getFavorite_teams());
        context.startActivity(intent);
    }
}

package com.example.tsthinh.testapp.view.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.tsthinh.testapp.home.R;
import com.example.tsthinh.testapp.presenter.home.HomePresenter;
import com.example.tsthinh.testapp.presenter.home.IHomePresenter;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.login.widget.LoginButton;

/**
 * Created by tsthinh on 11/7/2016.
 */

public class HomeActivity extends AppCompatActivity implements IHomeView {

    private IHomePresenter homePresenter;
    private LoginButton loginButton;
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_home);
        loginButton = (LoginButton) findViewById(R.id.login_button);
        homePresenter = new HomePresenter(this, this);
        callbackManager = CallbackManager.Factory.create();
        homePresenter.registerCallbackListener(loginButton, callbackManager);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        Log.d("Test", "Result code " + resultCode);
    }
}
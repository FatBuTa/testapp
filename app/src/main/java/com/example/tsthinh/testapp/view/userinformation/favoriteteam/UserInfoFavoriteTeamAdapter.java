package com.example.tsthinh.testapp.view.userinformation.favoriteteam;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tsthinh.testapp.home.R;
import com.example.tsthinh.testapp.model.userinformation.favoriteteam.FavoriteTeamModel;

import java.util.ArrayList;

public class UserInfoFavoriteTeamAdapter extends RecyclerView.Adapter<UserInfoFavoriteTeamAdapter.ViewHolder> {
    private ArrayList<FavoriteTeamModel> mDataset;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtName;

        public ViewHolder(View v) {
            super(v);
            txtName = (TextView) v.findViewById(R.id.team_name);
        }
    }

    public void add(int position, FavoriteTeamModel dataItem) {
        mDataset.add(position, dataItem);
        notifyItemInserted(position);
    }

    public UserInfoFavoriteTeamAdapter(ArrayList<FavoriteTeamModel> myDataset) {
        if (myDataset != null)
            mDataset = myDataset;
        else
            mDataset = new ArrayList<>();
    }

    @Override
    public UserInfoFavoriteTeamAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_info_favorite_team_item, parent, false);
        UserInfoFavoriteTeamAdapter.ViewHolder vh = new UserInfoFavoriteTeamAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(UserInfoFavoriteTeamAdapter.ViewHolder holder, int position) {
        final FavoriteTeamModel name = mDataset.get(position);
        holder.txtName.setText(mDataset.get(position).getName());

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}
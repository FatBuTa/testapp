package com.example.tsthinh.testapp.model.userinformation.favoriteteam;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by tsthinh on 11/8/2016.
 */

public class FavoriteTeamModel implements Serializable {
    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }
}

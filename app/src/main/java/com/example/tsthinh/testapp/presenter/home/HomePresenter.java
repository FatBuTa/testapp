package com.example.tsthinh.testapp.presenter.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.example.tsthinh.testapp.model.userinformation.UserInformationModel;
import com.example.tsthinh.testapp.view.home.IHomeView;
import com.example.tsthinh.testapp.view.userinformation.UserInformationActivity;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by tsthinh on 11/7/2016.
 */

public class HomePresenter implements IHomePresenter {
    final String TAG = "HomePresenter";
    private Context context;
    private IHomeView homeView;


    public HomePresenter(Context context, IHomeView homeView) {
        this.context = context;
        this.homeView = homeView;
    }

    @Override
    public void registerCallbackListener(LoginButton loginButton, CallbackManager callbackManager) {
        loginButton.setReadPermissions(Arrays.asList("public_profile", "user_friends", "email", "user_birthday",
                "user_about_me", "user_education_history", "user_location", "user_relationships",
                "user_relationship_details", "user_religion_politics", "user_website", "user_work_history", "user_likes"));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                Log.d(TAG, "All user info: " + response.toString());
                                Intent intent = new Intent(context, UserInformationActivity.class);
                                Gson gson = new GsonBuilder().create();
                                UserInformationModel userInformationModel = gson.fromJson(object.toString(), UserInformationModel.class);
                                intent.putExtra("UserInformation", userInformationModel);
                                context.startActivity(intent);
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,birthday,email,picture," +
                        "link,gender,political,public_key,quotes,relationship_status," +
                        "religion,third_party_id, work, education,favorite_teams");
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {
                Log.d(TAG, "user cancel login");
            }

            @Override
            public void onError(FacebookException exception) {
                Log.d(TAG, "Error " + exception.getMessage());
            }
        });
    }
}

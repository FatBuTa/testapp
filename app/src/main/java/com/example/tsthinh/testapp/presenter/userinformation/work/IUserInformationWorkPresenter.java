package com.example.tsthinh.testapp.presenter.userinformation.work;

import com.example.tsthinh.testapp.model.userinformation.work.JobModel;

import java.util.ArrayList;

/**
 * Created by tsthinh on 11/9/2016.
 */

public interface IUserInformationWorkPresenter {
    public ArrayList<JobModel> getJobModels();
}
